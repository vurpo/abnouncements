use std::{env, process::exit, time::Duration, str::FromStr};

use lettre::{transport::smtp::authentication::Credentials, Message, AsyncTransport};
use matrix_sdk::{
    config::SyncSettings,
    room::Room,
    ruma::{events::{room::message::{
        MessageType, RoomMessageEventContent, OriginalSyncRoomMessageEvent, TextMessageEventContent,
    }, SyncMessageLikeEvent}, OwnedRoomId, RoomId, api::client::{filter::FilterDefinition, sync::sync_events::v3::Filter}},
    Client, Session,
};
use once_cell::sync::OnceCell;
use regex::Regex;
use reqwest::Method;
use serde::Deserialize;
use lazy_static::lazy_static;
use serde_json::json;

#[derive(Debug, Deserialize)]
struct EmailCredentials {
    server: String,
    username: String,
    password: String
}

/// Authentication credentials for sending email
static EMAIL_CREDENTIALS: OnceCell<EmailCredentials> = OnceCell::new();

#[derive(Debug, Deserialize)]
struct AllowedUser {
    user_id: String,
    name: String
}

#[derive(Debug, Deserialize)]
struct MatrixSendConfig {
    enabled: bool,
    send_to_rooms: Vec<OwnedRoomId>
}

#[derive(Debug, Deserialize)]
struct EmailSendConfig {
    enabled: bool,
    send_from_email: String,
    send_to_emails: Vec<String>
}

#[derive(Debug, Deserialize)]
struct DiscordSendConfig {
    enabled: bool,
    send_to_webhooks: Vec<String>
}

#[derive(Debug, Deserialize)]
struct Config {
    homeserver_url: String,
    receive_rooms: Vec<OwnedRoomId>,
    allowed_users: Vec<AllowedUser>,
    matrix: MatrixSendConfig,
    email: EmailSendConfig,
    discord: DiscordSendConfig
}
/// Bot configuration
static CONFIG: OnceCell<Config> = OnceCell::new();

/// List of OwnedRoomId for what rooms the bot should see
/// (needs to be static because of filter things)
static FILTER_ROOMS_LIST: OnceCell<Vec<matrix_sdk::ruma::OwnedRoomId>> = OnceCell::new();

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room, client: Client) {
    let config = CONFIG.get().unwrap();
    lazy_static! {
        static ref ANNOUNCEMENT_REGEX: Regex = Regex::new(r"(?s)^ℹ️ ?(.+)\n(.+)").unwrap();
    }

    if let Room::Joined(room) = room {
        if config.receive_rooms.iter().find(|s| s == &room.room_id().as_str()).is_some() {
            
            let msg_body = match event.content.msgtype {
                MessageType::Text(TextMessageEventContent { body, .. }) => body,
                _ => return,
            };
            
            let sender = config.allowed_users.iter().find(|u| u.user_id == event.sender.as_str());
            if let Some(sender) = sender {
                if let Some(captures) = ANNOUNCEMENT_REGEX.captures(&msg_body) {
                    let subject: String = captures.get(1).unwrap().as_str().to_owned();
                    let body: String = captures.get(2).unwrap().as_str().to_owned();

                    println!("Matching message from {}!\n{}\n{}", sender.name, subject, body);

                    if config.matrix.enabled {
                        send_to_matrix(sender, &msg_body, &client).await;
                    }

                    if config.email.enabled {
                        send_to_email(sender, &subject, &body).await;
                    }

                    if config.discord.enabled {
                        send_to_discord(&sender, &msg_body).await;
                    }
                }
            }
        }
    }
}

async fn send_to_matrix(sender: &AllowedUser, msg_body: &str, client: &Client) {
    let config = CONFIG.get().unwrap();
    println!("Sending to {} Matrix rooms", config.matrix.send_to_rooms.len());

    let content = RoomMessageEventContent::text_plain(format!("From {}:\n{}", sender.name, msg_body));
    for room in &config.matrix.send_to_rooms {
        let joined_room = client.get_joined_room(&room);
        if let Some(send_to_room) = joined_room {
            let result = send_to_room.send(content.clone(), None).await;
        }
    }
}

async fn send_to_email(sender: &AllowedUser, subject: &str, body: &str) {
    let config = CONFIG.get().unwrap();
    println!("Sending to {} email addresses", config.email.send_to_emails.len());
    let email_credentials = EMAIL_CREDENTIALS.get().expect("No email credentials provided! See README.md");

    let smtp_credentials =
        Credentials::new(email_credentials.username.clone(), email_credentials.password.clone());
    let mailer = lettre::AsyncSmtpTransport::<lettre::Tokio1Executor>::relay(&email_credentials.server)
        .and_then(|b| Ok(b.credentials(smtp_credentials)))
        .and_then(|b| Ok(b.build::<lettre::Tokio1Executor>()));
    match mailer {
        Ok(mailer) => {
            for email_recipient in &config.email.send_to_emails {
                let email = Message::builder()
                    .from(config.email.send_from_email.parse().unwrap())
                    .to(email_recipient.parse().unwrap())
                    .subject(subject.clone())
                    .body(format!("{}\n\nSent by {}", body, sender.name));

                if let Ok(email) = email {
                    println!("{:?}", mailer.send(email).await);
                } else {
                    println!("Error creating email: {:?}", email);
                }
            }
        }
        Err(e) => {
            println!("Error creating mailer: {:?}", e);
        }
    }
}

async fn send_to_discord(sender: &AllowedUser, msg_body: &str) {
    let config = CONFIG.get().unwrap();
    println!("Sending to {} Discord webhooks", config.discord.send_to_webhooks.len());

    let client = reqwest::Client::builder().build().unwrap();
    for webhook_recipient in &config.discord.send_to_webhooks {
        let req = client.request(Method::POST, webhook_recipient)
            .json(&json!({
                "name": "AB-info",
                "type": 1i32,
                "content": format!("*From {}:*\n{}", sender.name, msg_body)
            }))
            .send().await;
    }
}

async fn start(
    session: Session,
) -> anyhow::Result<()> {
    #[allow(unused_mut)]
    let config = CONFIG.get().unwrap();
    let mut client_builder = Client::builder().homeserver_url(config.homeserver_url.clone());

    // The location to save files to
    let mut home = dirs::cache_dir().expect("no cache directory found");
    home.push("abnouncements");
    let home_ = home.clone();
    let state_store = matrix_sdk_sled::StateStore::open_with_path(home)?;
    let crypto_store = matrix_sdk_sled::CryptoStore::open_with_passphrase(home_, None)?;
    client_builder = client_builder.state_store(state_store).crypto_store(crypto_store);

    let client = client_builder.build().await.unwrap();
    client.restore_login(session).await?;
    let token = client.store().get_sync_token().await?;

    eprintln!("Logged in. Wait while catching up to the current messages...");
    eprintln!("Receiving messages from rooms: {:?}", config.receive_rooms);

    let mut filter_rooms: Vec<OwnedRoomId> = Vec::new();
    filter_rooms.append(&mut config.receive_rooms.clone());
    filter_rooms.append(&mut config.matrix.send_to_rooms.clone());
    FILTER_ROOMS_LIST.set(filter_rooms).unwrap();
    let filter_rooms = FILTER_ROOMS_LIST.get().unwrap();

    let mut filter = FilterDefinition::default();
    filter.room.rooms = Some(filter_rooms);

    let filter_id = client
        .get_or_upload_filter("sync", filter)
        .await
        .unwrap();

    // An initial sync to set up state and so our bot doesn't respond to old
    // messages. If the `StateStore` finds saved state in the location given the
    // initial sync will be skipped in favor of loading state from the store
    let syncsettings = if let Some(token) = token {
        SyncSettings::new()
            .timeout(Duration::from_secs(60))
            .filter(Filter::FilterId(&filter_id))  
            .token(token)
    } else {
        SyncSettings::new()
            .timeout(Duration::from_secs(60))
            .filter(Filter::FilterId(&filter_id))
    };

    while let Err(e) = client.sync_once(syncsettings.clone()).await {
        println!("Retrying first sync: {:?}", e);
    }
    // add our CommandBot to be notified of incoming messages, we do this after the
    // initial sync to avoid responding to messages before the bot was running.
    client.register_event_handler(on_room_message).await;

    // since we called `sync_once` before we entered our sync loop we must pass
    // that sync token to `sync`
    let settings = SyncSettings::default()
        .filter(Filter::FilterId(&filter_id))
        .token(client.sync_token().await.unwrap());
    // this keeps state from the server streaming in to CommandBot via the
    // EventHandler trait
    println!("Starting bot!");
    client.sync(settings).await;

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    let (login_file, email_credentials_file, config_file) =
        ("matrix_login.json", "email_credentials.json", "config.json");
        // TODO: add a real command line options library
/*         match (env::args().nth(1), env::args().nth(2), env::args().nth(3)) {
            (Some(a), Some(b), Some(c)) => (a, b, c),
            _ => {
                eprintln!(
                    "Usage: {} <matrix_login_file> <email_credentials_file> <config_file>",
                    env::args().next().unwrap()
                );
                exit(1)
            }
        }; */
    
    let config_file_json = std::fs::read_to_string(&config_file).expect("Unable to read config file");
    let config: Config = serde_json::from_str(&config_file_json).expect("Can't parse config file contents");
    CONFIG.set(config).unwrap();

    let email_credentials =
        std::fs::read_to_string(&email_credentials_file)
        .map_err(|e| format!("Couldn't open email credentials file: {}", e))
        .and_then(|string| {
            serde_json::from_str::<EmailCredentials>(&string)
            .map_err(|e| format!("Error reading email credentials file: {}", e))
        });
    
    match email_credentials {
        Ok(email_credentials) => EMAIL_CREDENTIALS.set(email_credentials).unwrap(),
        Err(error) => eprintln!("{}\nThis is okay if you're not using the email feature.", error)
    }
    
    let login_file_json = std::fs::read_to_string(&login_file).expect("Unable to read login file");
    let session: Session = serde_json::from_str(&login_file_json).expect("Can't parse Matrix login file contents");

    match start(session).await {
        Ok(_) => (),
        Err(e) => {
            eprintln!("{}", e.to_string());
            eprintln!("{}", e.backtrace());
        },
    }
    Ok(())
}