# ABnouncements

A simple bot for forwarding announcement messages from one Matrix room to other Matrix rooms, and also to email and Discord webhooks.

# Setup

Use [matrix_login](https://gitlab.com/vurpo/matrix-login/) to sign in to a Matrix account, and name the login file `matrix_login.json`. (Alternatively, manually create a JSON file with an object containing the keys `access_token`, `user_id`, and `device_id`.)

Copy `config.json.sample` to `config.json` and fill in the fields correctly. The `enabled` flags are used to enable *sending to* Matrix rooms, email addresses, and Discord webhooks.

If you are going to send to email, also copy `email_credentials.json.sample` to `email_credentials.json` and fill in the SMTP server, username, and password through which you're going to send the email. This file is not needed if email sending is disabled.